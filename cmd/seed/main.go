package main

import (
	"WomenValidatorDemo/database/seed"
	"fmt"
	"time"
)

// Первичное заполнение базы данных случайными данными
func main() {
	defer elapsed()()

	seed.Boot()
}

func elapsed() func() {
	start := time.Now()

	return func() {
		fmt.Printf("\n\nDONE seeding at %s", time.Since(start))
	}
}
