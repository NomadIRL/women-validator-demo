package main

import (
	"WomenValidatorDemo/internal/stubs"
	"fmt"
)

// Отладка склонятора отчества...
func main() {
	fmt.Println("*************--МУЖСКОЕ--*************")
	for _, name := range stubs.MaleNames {
		middleName := stubs.MorphMiddleName(name, true)

		fmt.Println(name + "=>" + middleName)
	}
	fmt.Println("*************--ЖЕНСКОЕ--*************")
	for _, name := range stubs.MaleNames {
		middleName := stubs.MorphMiddleName(name, false)

		fmt.Println(name + "=>" + middleName)
	}
}
