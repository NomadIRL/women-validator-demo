package models

import "database/sql"

const (
	SocialGroupTypeNews          = 1 // Новости
	SocialGroupTypeCelebrities   = 2 // Знаменитости
	SocialGroupTypePsychology    = 3 // Психология и отношения
	SocialGroupTypeEntertainment = 4 // Развлечения
	SocialGroupTypeFamilyAndKids = 5 // Семья и дети
	SocialGroupTypeOther         = 6 // Другое
)

type SocialGroup struct {
	Id          uint64 // ID Группы
	Name        string // Название группы
	Description string // Описание группы
	Type        int    // Тип группы
}

func (s SocialGroup) ExtractAll(rows *sql.Rows) ([]Model, error) {
	var socialGroups []Model

	for rows.Next() {
		sg := SocialGroup{}

		err := rows.Scan(&sg.Id, &sg.Name, &sg.Description, &sg.Type)
		if err != nil {
			return nil, err
		}

		socialGroups = append(socialGroups, sg)
	}

	return socialGroups, nil
}
