package models

import "database/sql"

const (
	RelationTypeChild   = 1 // Родитель-ребёнок
	RelationTypeFriend  = 2 // Друг-друг
	RelationTypeBelover = 3 // Любовник-любовник
	RelationTypeSpouse  = 4 // Супруг-супруг
)

// HumanRelationship из таблицы human_relationships
type HumanRelationship struct {
	Id        uint64  // ID записи об отношениях
	Human1    uint64  // Инициатор отношений
	Human2    uint64  // Человек, вошедший в отношения
	Type      uint8   // Тип отношений
	DateStart string  // Дата начала отношений
	DateEnd   *string // Дата окончания отношений
}

func (m *HumanRelationship) ExtractAll(rows *sql.Rows) ([]Model, error) {
	return nil, nil
}
