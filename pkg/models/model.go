package models

import "database/sql"

const (
	DateLength = 10 // Длина строки для Даты: [0, 10) - '2021-12-31'

	HumansTableName             = "humans"
	HumanRelationShipsTableName = "human_relationships"
	HumanDiseasesTableName      = "human_diseases"
	SocialGroupsTableName       = "social_groups"
)

type Model interface {
	ExtractAll(rows *sql.Rows) ([]Model, error)
}
