package Server

import (
	"context"
	"log"
	"net/http"
	"os"
	"time"
)

type Server struct {
	httpServer *http.Server
}

// Run Запустить сервер
func (server *Server) Run(routes http.Handler) {
	port := os.Getenv("SERVER_PORT")

	server.httpServer = &http.Server{
		Addr:           ":" + port,
		Handler:        routes,
		MaxHeaderBytes: 1 << 20, // 1 MB
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
	}

	err := server.httpServer.ListenAndServe()

	if err != nil {
		log.Fatal("Unable to start server on port " + port)
	}
}

// Shutdown Остановить сервер
func (server *Server) Shutdown(context context.Context) {
	err := server.httpServer.Shutdown(context)

	if err != nil {
		log.Fatal("Unable to stop server; error: " + context.Err().Error())
	}
}
