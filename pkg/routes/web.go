package routes

import (
	render "WomenValidatorDemo/pkg/handlers/web"
	"github.com/gin-gonic/gin"
)

// Подгрузить роуты, которые выполняют render
func collectWebRoutes(router *gin.Engine) {
	web := router.Group("/")

	web.GET("", render.RenderMainPage)
}
