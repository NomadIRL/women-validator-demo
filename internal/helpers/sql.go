package helpers

import (
	"fmt"
	"github.com/qustavo/dotsql"
	"sort"
)

//ExecSqlFile Выполняет файл с инструкциями sql, не предполагающими возврат каких-либо результатов
func ExecSqlFile(db dotsql.Execer, path string, params map[string][]interface{}) error {
	sqlFile, _ := dotsql.LoadFromFile(path)

	var sqlNames []string

	for sqlName := range params {
		sqlNames = append(sqlNames, sqlName)
	}

	sort.Strings(sqlNames)

	for _, name := range sqlNames {
		_, err := sqlFile.Exec(db, name, params[name]...)
		if err != nil {
			return fmt.Errorf("failed to exec sql %s at %s: %s", path, name, err.Error())
		}
	}

	return nil
}
