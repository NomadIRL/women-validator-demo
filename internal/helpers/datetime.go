package helpers

import (
	"fmt"
	"log"
	"math/rand"
	"time"
)

var DateFormat = time.RFC3339

// GetRandomDateBetweenStrings Возвращает случайную дату в виде time.Time и "2006-01-02T15:04:05Z07:00";
// если end не указан (nil), то максимальной датой считается сегодняшний день
func GetRandomDateBetweenStrings(start string, end *string) (time.Time, string) {
	format := DateFormat

	startTime, err := time.Parse(format, start)
	if err != nil {
		log.Fatal(fmt.Sprintf("Unable to resolve start date %s in format %s: %s", start, format, err.Error()))
	}

	var endTime time.Time

	if end != nil {
		endTime, err = time.Parse(format, *end)
		if err != nil {
			log.Fatal(fmt.Sprintf("Unable to resolve end date %s in format %s: %s", *end, format, err.Error()))
		}
	} else {
		endTime = time.Now()
	}

	delta := endTime.Unix() - startTime.Unix()
	sec := rand.Int63n(delta) + startTime.Unix()
	randDate := time.Unix(sec, 0)

	return randDate, randDate.Format(format)
}
