package seed

import (
	"WomenValidatorDemo/internal/helpers"
	"WomenValidatorDemo/pkg/models"
	"fmt"
	"github.com/jmoiron/sqlx"
	"log"
	"path"
	"sync"
)

type humanRelationships struct {
	db *sqlx.DB
}

func (h humanRelationships) seed(db *sqlx.DB, wg *sync.WaitGroup) {
	defer wg.Done()

	h.db = db

	var relSeedWg sync.WaitGroup
	relSeedWg.Add(3)

	h.createChildRelations()
	go h.createFriendRelations(&relSeedWg)
	go h.createBeloverRelations(&relSeedWg)
	go h.createMarriageRelations(&relSeedWg)

	relSeedWg.Wait()

	fmt.Println("Human relationships seeded OK")
}

// Все, для кого можно найти родителя по критерию, обязаны иметь родителей; минимальный возраст родителя - 18 лет
func (h *humanRelationships) createChildRelations() {
	sqlPath := path.Join("database", "seed", "sql", "human_relationships_create_families.sql")

	sqlParams := map[string][]interface{}{
		"1step": {},
		"2step": {models.RelationTypeChild},
	}

	err := helpers.ExecSqlFile(*h.db, sqlPath, sqlParams)
	if err != nil {
		log.Fatal(err.Error())
	}

	fmt.Println(`RELATIONS: Families seeded`)
}

// Друзья начинают фиксироваться с 12 лет
func (h *humanRelationships) createFriendRelations(relSeedWg *sync.WaitGroup) {
	defer relSeedWg.Done()

	sqlPath := path.Join("database", "seed", "sql", "human_relationships_create_non_relatives.sql")

	minAge := 12
	relType := models.RelationTypeFriend
	corruptChance := 0.5
	variations := 0.98
	bipolar := false

	//goland:noinspection GoBoolExpressions
	sqlParams := map[string][]interface{}{
		"1step": {minAge},
		"2step": {minAge, relType, corruptChance, variations, bipolar},
	}

	err := helpers.ExecSqlFile(*h.db, sqlPath, sqlParams)
	if err != nil {
		log.Fatal(err.Error())
	}

	fmt.Println(`RELATIONS: Friends seeded`)
}

// Любовные отношения фиксируются с 16 лет
func (h *humanRelationships) createBeloverRelations(relSeedWg *sync.WaitGroup) {
	defer relSeedWg.Done()

	sqlPath := path.Join("database", "seed", "sql", "human_relationships_create_non_relatives.sql")

	minAge := 16
	relType := models.RelationTypeBelover
	corruptChance := 0.9
	variations := 0.99
	bipolar := true

	//goland:noinspection GoBoolExpressions
	sqlParams := map[string][]interface{}{
		"1step": {minAge},
		"2step": {minAge, relType, corruptChance, variations, bipolar},
	}

	err := helpers.ExecSqlFile(*h.db, sqlPath, sqlParams)
	if err != nil {
		log.Fatal(err.Error())
	}

	fmt.Println(`RELATIONS: Belovers seeded`)
}

// Брак возможен только с 18 лет; дети могут быть рождены вне брака
func (h *humanRelationships) createMarriageRelations(relSeedWg *sync.WaitGroup) {
	defer relSeedWg.Done()

	sqlPath := path.Join("database", "seed", "sql", "human_relationships_create_non_relatives.sql")

	minAge := 18
	relType := models.RelationTypeSpouse
	corruptChance := 0.25
	variations := 0.98
	bipolar := true

	//goland:noinspection GoBoolExpressions
	sqlParams := map[string][]interface{}{
		"1step": {minAge},
		"2step": {minAge, relType, corruptChance, variations, bipolar},
		"3step": {relType},
	}

	err := helpers.ExecSqlFile(*h.db, sqlPath, sqlParams)
	if err != nil {
		log.Fatal(err.Error())
	}

	fmt.Println(`RELATIONS: Spouses seeded`)
}
