-- Все прежние отношения между людьми удаляются для поддержания корректности данных
-- name: 1step
TRUNCATE TABLE human_relationships RESTART IDENTITY;

/*
 Для каждого ребенка мы подбираем случайных и совершеннолетних отца и мать.
 Слишком старые люди не будут иметь данные о родителях, т.к. объем тестовых данных ограничен
*/
-- name: 2step
INSERT INTO human_relationships
SELECT DISTINCT ON(children.id, humans.gender)
    nextval('human_relationships_id_seq'),
    humans.id,
    children.id,
    $1 AS type,
    children.birth_date,
    children.death_date
FROM humans, (SELECT id, birth_date, death_date from humans) as children
WHERE humans.birth_date < date(children.birth_date) - INTERVAL '18' YEAR
  AND (humans.death_date IS NULL OR date(humans.death_date) > date(children.birth_date))
ORDER BY children.id, humans.gender, random();