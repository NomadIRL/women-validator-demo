package seed

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"sync"
)

type socialGroupUsers struct{}

func (s socialGroupUsers) seed(db *sqlx.DB, wg *sync.WaitGroup) {
	defer wg.Done()

	fmt.Println("Social group users seeded OK")
}
