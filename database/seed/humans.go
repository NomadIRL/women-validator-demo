package seed

import (
	"WomenValidatorDemo/internal/helpers"
	"WomenValidatorDemo/internal/stubs"
	"WomenValidatorDemo/pkg/models"
	"fmt"
	"github.com/jmoiron/sqlx"
	"log"
	"math/rand"
	"path"
	"sync"
	"time"
)

const (
	MaxAge           = 90   // Максимальный возраст человека в базе данных
	TotalHumansLimit = 1000 // Кол-во генерируемых людей
)

type humans struct {
	db *sqlx.DB
}

func (h *humans) seed(db *sqlx.DB, _ *sync.WaitGroup) {
	h.db = db

	// Перед заполнением людей БД полностью очищается
	h.cleanDb()

	var rowsWg sync.WaitGroup
	rowsWg.Add(TotalHumansLimit)

	for i := 0; i < TotalHumansLimit; i++ {
		// Каждый 10-й INSERT идёт синхронно, чтобы соединения успевали закрываться
		if i%10 == 0 {
			h.handleOneRow(&rowsWg)
		} else {
			go h.handleOneRow(&rowsWg)
		}
	}

	rowsWg.Wait()

	fmt.Println("Humans seeded OK")
}

func (h *humans) cleanDb() {
	sqlPath := path.Join("database", "seed", "sql", "clean_db.sql")

	sqlParams := map[string][]interface{}{
		"1step": {},
	}

	err := helpers.ExecSqlFile(*h.db, sqlPath, sqlParams)
	if err != nil {
		log.Fatal(err.Error())
	}
}

func (h *humans) generate() *models.Human {
	var name string
	var middleName string
	var deathDate *string

	gender := rand.Intn(2) == 1
	surname := stubs.Surnames[rand.Intn(len(stubs.Surnames))]
	fatherName := stubs.MaleNames[rand.Intn(len(stubs.MaleNames))]
	age := rand.Intn(MaxAge)
	birthDateRaw := time.Now().AddDate(-age, -rand.Intn(13), -rand.Intn(32))
	birthDate := birthDateRaw.String()[0:models.DateLength]
	isDead := rand.Intn(100) < 2 // Вечные 2% 💩

	if isDead {
		maxDaysAlive := int(time.Now().Sub(birthDateRaw).Hours() / 24)

		deathDateRaw := birthDateRaw.AddDate(0, 0, rand.Intn(maxDaysAlive+1)).String()
		deathDateRaw = deathDateRaw[0:models.DateLength]

		deathDate = &deathDateRaw
	}

	if gender {
		name = stubs.MaleNames[rand.Intn(len(stubs.MaleNames))]
	} else {
		name = stubs.FemaleNames[rand.Intn(len(stubs.FemaleNames))]
	}

	middleName = stubs.MorphMiddleName(fatherName, gender)

	m := models.Human{
		Gender:     gender,
		Name:       name,
		Surname:    surname,
		MiddleName: middleName,
		BirthDate:  birthDate,
		DeathDate:  deathDate,
	}

	return &m
}

func (h *humans) insert(model *models.Human) {
	sql := fmt.Sprintf(
		`INSERT INTO %s (name, surname, middle_name, gender, birth_date, death_date) values ($1,$2,$3,$4,$5,$6)`,
		models.HumansTableName,
	)

	_, err := h.db.Exec(sql, model.Name, model.Surname, model.MiddleName, model.Gender, model.BirthDate, model.DeathDate)

	if err != nil {
		fmt.Printf("Error during insert a sql '%s': %s; \n", sql, err.Error())
	}
}

func (h *humans) handleOneRow(rowsWg *sync.WaitGroup) {
	defer rowsWg.Done()

	model := h.generate()
	h.insert(model)
}
